# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=element-desktop
pkgver=1.11.4
pkgrel=0
pkgdesc="Secure and independent communication, connected via Matrix"
url="https://element.io/"
arch="aarch64 x86_64" # same as electron
license="Apache-2.0"
depends="electron"
makedepends="
	cargo
	electron-dev
	libsecret-dev
	nodejs-current
	npm
	python3
	sqlcipher-dev
	yarn
	"
source="
	https://github.com/vector-im/element-desktop/archive/refs/tags/v$pkgver/element-desktop-$pkgver.tar.gz
	https://github.com/vector-im/element-web/archive/refs/tags/v$pkgver/element-web-$pkgver.tar.gz

	desktop-add-alpine-target.patch
	desktop-use-system-headers.patch

	element-desktop
	element-desktop.desktop
	"
options="!check" # broken

# secfixes:
#   1.11.4-r0:
#     - CVE-2022-36059
#     - CVE-2022-36060

# used by buildscripts (at least web's webpack)
export VERSION=$pkgver

export CARGO_PROFILE_RELEASE_PANIC=abort
export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_OPT_LEVEL=2
export CARGO_PROFILE_RELEASE_STRIP="symbols"
export NODE_OPTIONS="--openssl-legacy-provider"

prepare() {
	msg "Applying patches"
	for x in $source; do
		case "$x" in
		web-*.patch)
			msg "$x"
			patch -p1 -i "$srcdir"/$x -d "$srcdir"/element-web-$pkgver
			;;
		desktop-*.patch)
			msg "$x"
			patch -p1 -i "$srcdir"/$x -d "$srcdir"/element-desktop-$pkgver
			;;
		esac
	done

	(
		cd "$srcdir"/element-web-$pkgver

		msg "Fetch element-web dependencies"
		yarn install --frozen-lockfile --ignore-scripts
		cp config.sample.json config.json
	)

	ln -s "$srcdir"/element-web-$pkgver/webapp webapp

	msg "Fetch element-desktop dependencies"
	yarn install --frozen-lockfile --ignore-scripts
}

build() {
	(
		cd "$srcdir"/element-web-$pkgver

		msg "Build element-web"
		NODE_ENV=production yarn build
	)

	msg "Build element-desktop"

	yarn asar-webapp

	# add "optional" native dependencies
	# hak stands for hack
	yarn run hak --target "$(uname -m)-alpine-linux-musl"

	# stripping in build because it gets into asar
	strip node_modules/keytar/build/Release/keytar.node

	yarn build:ts

	yarn build:res

	yarn electron-builder \
		--config.directories.output=release \
		--linux=dir
}

check() {
	(
		cd "$srcdir"/element-web-$pkgver

		yarn test
	)
}

package() {
	mkdir -p "$pkgdir"/usr/lib/element-desktop/img

	local path_arch=''
	if [ "$(uname -m)" = "aarch64" ]; then
		path_arch='-arm64'
	fi
	local resources="release/linux$path_arch-unpacked/resources"

	install -Dm644 $resources/app.asar "$pkgdir"/usr/lib/element-desktop/
	install -Dm644 $resources/webapp.asar "$pkgdir"/usr/lib/element-desktop/

	cp -r $resources/app.asar.unpacked "$pkgdir"/usr/lib/element-desktop/

	install -Dm644 $resources/img/* "$pkgdir"/usr/lib/element-desktop/img/

	install -Dm755 "$srcdir"/$pkgname "$pkgdir"/usr/bin/$pkgname
	install -Dm644 "$srcdir"/$pkgname.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop

	for i in 16 24 48 64 96 128 256; do
		install -Dm644 "$builddir"/build/icons/${i}x$i.png "$pkgdir"/usr/share/icons/hicolor/${i}x$i/apps/element.png
	done
}

sha512sums="
d0f9708e09fdf68373d7e32b32f840b27882c6a2ba76aa442e19cc17579ed66b62333f0cb2c3fc0686705990bb3ab9345fefe113db4fe4cc411fc565497afbcf  element-desktop-1.11.4.tar.gz
01d0d3c7e9af810d4ad50db17eb18ded3d2403be72210930a80c2b3c72feba01de5303b87c187ba6d6d3e19d3987cc178c97dadc280a40c97db37952fd23236f  element-web-1.11.4.tar.gz
00e15ad47fe025c7f8c25df4593916bb095526aa8388d5a1e5d12bba866ca60c647a406bb046a29aedda264c9d681dcc8a8aaa66ebfe643c68f5dc1913d7f0ec  desktop-add-alpine-target.patch
3b85022104893344188790642095b8bd548545efa88ec2641002c3768f79783f3db220ea5d39b77bbc0cea38257ee9d3358ec04fd5e4bc64b698b68e52eb8778  desktop-use-system-headers.patch
bb65e4ddcb8542f7190db03a6bda9ce2f39044e34304fe0d16b1940a78594f66ea8a7ab447f39229d894c0d2a8cb160eef0c8dce6d36949da70aa899311bf333  element-desktop
d4f88f2652e3ac88cf1c218e0fa72d63caf65441a713b09b318943bac9b51eff8e37c48d3452faf2409545406ea0be438b03bd21bf9857ea1955843958e302b6  element-desktop.desktop
"
